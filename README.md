# Versedbot Discord Bot created by Andy Banh

## Note From the Creator

If you've somehow stumbled across this bot while scouring the internet, this was bot was created as a learning experience from a fresh college graduate. Its functions were created with the intent to assist users in discord servers with managing events, easily retrieving data from the internet, and increasing server security. I do not condone the use of this bot to abuse any public or private domains, and I ask that you as someone who may be interested in using this bot to please share the same mindset. Thank you.

## Current Features

Role Management - Allows users to set roles they wish without the help of an admin.

Pricewatch - Webscraping function that can track and store stock and price changes for products across different marketplace websites. 

FFMPEG Playback - Plays audio from a stream or video link to the voice channel the bot is currently in.

Secret Santa - Users opt in and post a list of items they want for Christmas. The bot then distributes names and everyone buys a gift for their drawn name.

## How to run

Download the init.py file and place it inside of a folder. Create a new text file and change the name to be blank and the extension to .env. Place the following lines inside of the file.

DISCORD_TOKEN = XXXXX

DISCORD_GUILD

VIRUSTOTAL_KEY = XXXXX

Replace the XXXXX with their corresponding values which can be obtained from the Discord Developer Portal and VirusTotal's Account Profile APIs. Leave DISCORD_GUILD blank as I have not implemented any functionalities using it yet. You may also leave VIRUSTOTAL_KEY blank but be aware that this will currently cause errors to be sent to the console whenever a link is posted.

## Core Functionalities & Details

Storage - All files are stored locally in whichever location the .py file of the bot is placed at. No files or data will be sent online or to me. 
The files are as follows:

- Botvar.txt - This currently only holds the channel that the bot reads commands from. Can be editted with a notepad or using !!setchannel on your server
- Botdata.txt - Holds almost all of the internal data structure variables that cannot be written to a file normally using python's out. Please do not modify this file if you don't know what you're doing as is it dumped using the Pickle library. Any edits to it may cause the bot to stop functioning or throw errors as the data will be corrupted. 
- PriceHistory.txt - Holds all of the data regarding past prices scanned by the Pricewatch feature. This DOES NOT contain the links or users that are currently tracking price or stock through Pricewatch. That data is stored in botdata.txt. I separated this incase the bot host doesn't want to store any price data on their machine.
- Whitelist.txt - A list of sites to be ignored by the bot so that it isn't sent to VirusTotal for scanning. Edittable using notepad or the VirusTotal related commands.

## Pricewatch Important Notes

This bot currently scans down a list of links that users on your server provide. The default wait timer inbetween scans is 5 minutes, but this can be safely lowered down to around 2-3 seconds depending on how long you plan to run the bot for. The current list of supported sites are: Newegg.com, Amazon.com, Microcenter.com, BHPhotoVideo.com, and Hottopic.com.

Each link does not hold independent timers, instead they are scanned in order with 5 minutes inbetween each. This will be changed in the future to hold independent timers for each domain rather than all links sharing the same timer.

## VirusTotal Important Notes

Requires you to provide a VirusTotal key to use. It is completely free to obtain from their websites just by making an account.

This feature only supports links, as file scanning would require the machine running the bot to download the file in order to pass to VirusTotal.

VirusTotal is community based, meaning that if a link is new or has never been run through their screening process before, then there will be no result for the bot to determine whether or not the link is malicious. The bot also only scans the root domain (e.g. youtube.com/watch... will only scan youtube.com) as anything further than that will most likely never have been scanned by VirusTotal.

## To be implemented

- Completion of Secret Santa features.
- Changing Pricewatch to have domain based timers.
- Implementation of SQLite for database storage over Pickle-dumped txt files.
- Bot hosted serverwide game night events.
