import os
import random
import discord
import string
import pickle
import requests
import matplotlib.pyplot as plt
import numpy as np
import re
import youtube_dl
import base64
import copy
from bs4 import BeautifulSoup
from asyncio import sleep
from discord.utils import get
from discord.ext import commands
from dotenv import load_dotenv
from discord import role
from collections import OrderedDict
from _datetime import datetime
from matplotlib.dates import (DAILY, DateFormatter, rrulewrapper, RRuleLocator, drange)
from statistics import mode
from pickle import NONE
from _operator import contains
from urllib.parse import urlparse
from copy import deepcopy
from numpy.matlib import rand

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
VT_KEY = os.getenv('VIRUSTOTAL_KEY')

bot = commands.Bot(command_prefix='!!')
bot.SAVEFLAG = True #Used in Pricewatch to prevent exceptions from concurrent modification of an instance when trying to delete links.
bot.CHANNEL = 0 #The text channel the bot reads commands from. Bot will not work if the channel is not set.
bot.ROLE_LIST = []
bot.PRICE_LINKS = set() #Product links to be scanned by pricewatch.
bot.PRICE_USERS = dict()  #Users who wish to be notified by Pricewatch along with their corresponding link(s) and desired price.
bot.PRICE_HISTORY = dict()
bot.NOTIFIED_LINKS = set() 
bot.HIGH_NOTIFY = set() #Users who wish to receive a PM when being mentioned in the server by the bot
bot.LINKS_TOREMOVE = set()
bot.LINKS_TOADD = set()
bot.WHITELISTED_SITES = set() #Websites that are considered safe by the admins and will be ignored by the bot for scanning.
bot.AUDIO_QUEUE = list()
bot.CURR_VC_CHANNEL = 0 #The voice channel the bot is currently in. Used for FFMPEG playback
bot.SS_OPTED = set() #Users who are opted into Secret Santa.
bot.SS_PAIRS = dict()
bot.SS_WISHLISTS = dict() #Secret Santa wishlists.
bot.SS_STARTED = False

headers = {"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0", 
           "Accept-Encoding":"gzip, deflate",
           "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", 
           "DNT":"1","Connection":"close", 
           "Upgrade-Insecure-Requests":"1"} #For use with Pricewatch to get past basic bot filters.

#########################################

# INITIALIZATION & CORE FUNCTIONALITIES #

#########################################

#Loads in files and starts up all tasks that are supposed to be automated. 
#The automated tasks are currently: Pricewatch and FFMPEG playback.
#If any files are missing, this will call save_task() which will regenerate the files.
@bot.event
async def on_ready():
    try:
        with open('botvar.txt','r') as f:
            bot.CHANNEL = int(f.readline())
            f.close()
        with open('botdata.txt','rb') as g:
            bot.ROLE_LIST = pickle.load(g)
            bot.PRICE_LINKS = pickle.load(g)
            bot.PRICE_USERS = pickle.load(g) 
            bot.HIGH_NOTIFY = pickle.load(g)
            bot.LINKS_TOREMOVE = pickle.load(g)
            bot.LINKS_TOADD = pickle.load(g)
            g.close()
        with open('pricehistory.txt','rb') as h:
            bot.PRICE_HISTORY = pickle.load(h)
            h.close()
        with open('whitelist.txt','r') as i:
            links = i.readlines()
            for x in links:
                x = x.replace('\n','')
                bot.WHITELISTED_SITES.add(x)
            i.close()
    except:
        print('One or more files do not exist, generating missing files...')
    bot.loop.create_task(save_task(bot))
    bot.loop.create_task(playback(bot))
    bot.loop.create_task(modifyLinks(bot))
    await sleep(3)
    bot.loop.create_task(webscrape(bot))

#This handles any messages sent with an url to be sent to VirusTotal for scanning if it is not on the whitelist.     
@bot.event    
async def on_message(message):
    if re.search("(?P<url>https?://[^\s]+)", message.content) is None:
        await bot.process_commands(message)
        return
    else:
        link = re.search("(?P<url>https?://[^\s]+)", message.content).group("url")
    if message.author.id == bot.user.id:
        return
    if link is not None:
        for x in bot.WHITELISTED_SITES:
            if x in link:
                await bot.process_commands(message)
                return
        headers = {'X-Apikey': VT_KEY}
        link = urlparse(link).netloc
        link = "https://" + link + "/"
        link = base64.urlsafe_b64encode(link.encode()).decode().strip("=")
        link = "https://www.virustotal.com/api/v3/urls/" + link
        response = requests.get(link, headers=headers).json()
        stats = response.get('data').get('attributes').get('last_analysis_stats')
        print(stats)
        if((int(stats.get('malicious')) > 0 and int(stats.get('malicious')) < 3) or (int(stats.get('suspicious')) > 0 and int(stats.get('suspicious')) < 3)):
            await bot.process_commands(message)
            await message.author.channel.send('Hi there, just wanted to let everyone know that the link above may or may not contain malware or suspicious activity. VirusTotal has reported at least 1 malicious or 1 suspicious activity on this site')
        elif(int(stats.get('malicious')) >= 3 or int(stats.get('suspicious')) >= 3):
            await message.author.channel.send('Hi there, VirusTotal has reported at least 3 malicious or 3 suspicious activity on this site. For safety purposes, I will be removing this link.')
            await message.delete()
        else:
            await bot.process_commands(message)
            
@bot.event
async def on_disconnect():
    print("Bot has disconnected.")

#Saves all current variables to textfiles.
async def save_task(bot):
    while True:
        if bot.SAVEFLAG:
            with open('botvar.txt','w') as f:
                f.write(bot.CHANNEL.__str__())
                f.close()
            with open('botdata.txt','wb') as g:
                pickle.dump(bot.ROLE_LIST,g)
                pickle.dump(bot.PRICE_LINKS,g)
                pickle.dump(bot.PRICE_USERS,g)
                pickle.dump(bot.HIGH_NOTIFY,g)
                pickle.dump(bot.LINKS_TOREMOVE,g)
                pickle.dump(bot.LINKS_TOADD,g)
                pickle.dump(bot.SS_OPTED,g)
                pickle.dump(bot.SS_WISHLISTS,g)
                g.close()
            with open('pricehistory.txt','wb') as h:
                pickle.dump(bot.PRICE_HISTORY,h)
                h.close()
            with open('whitelist.txt','w') as i:
                for x in bot.WHITELISTED_SITES:
                    i.write(x + '\n')
                i.close()
        await sleep(1)
        
######################################

#  DISCORD SERVER RELATED FUNCTIONS  #

######################################

#Sets the channel that the bot will read in commands from. The bot will not work at all if this command is never run.
@bot.command(name='setchannel', help= 'Sets the channel that the bot will only read commands from.')
async def setChannel(ctx):
    bot.CHANNEL = ctx.message.channel.id
    await ctx.send('VersedBot will now only read commands from this channel.')

#ADMIN ONLY. Makes a role available to be set through the bot by any user.   
@bot.command(name='addrole', help= 'Adds the role onto the list of roles available to be picked by any members of this server.')
async def addRole(ctx, rolename):
    if ctx.author.guild_permissions.administrator:
        if ctx.message.channel.id != bot.CHANNEL:
            return
        if get(ctx.message.guild.roles, name=rolename) == None and any(char.isdigit() for char in rolename) == False:
            await ctx.send('This role does not exist')
        else:
            #Handles all four possible options of typing just the role name, role mention, role id, or mention id. Works exactly the same as in getRole, remRole, and delRole.
            if any(char.isdigit() for char in rolename):
                if bot.ROLE_LIST.count(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).id):
                    await ctx.send('This role is already on the selectable role list.')
                    return
                else:
                    bot.ROLE_LIST.append(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).id)
                    await ctx.send("Role " + get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).name + " added to list of selectable roles.")
            elif bot.ROLE_LIST.count(get(ctx.message.guild.roles, name=rolename).id):
                await ctx.send('This role is already on the selectable role list.')
                return
            else:
                bot.ROLE_LIST.append(get(ctx.message.guild.roles, name=rolename).id)
                await ctx.send("Role " + get(ctx.message.guild.roles, name=rolename).name + " added to list of selectable roles.")
    else:
        await ctx.send(ctx.author.mention + ", This command can only be run by an administrator.")

#Gives a command caller their selected role from the list of roles created by addRole().               
@bot.command(name='getrole', help= 'Allows any users to give themselves any role on the bot\'s role list.')  
async def getRole(ctx, rolename):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if get(ctx.message.guild.roles, name=rolename) == None and any(char.isdigit() for char in rolename) == False:
        await ctx.send('This role does not exist on the list of selectable roles')
    else:
        if any(char.isdigit() for char in rolename):
            if bot.ROLE_LIST.count(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).id):
                if(ctx.message.author.roles.count(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename))))) == 0):
                    await discord.Member.add_roles(ctx.message.author,get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))))
                    await ctx.send(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).name + ' assigned to ' + ctx.message.author.mention + ".")
                else:
                    await ctx.send('You already have the role ' + get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).name + ".")
        elif bot.ROLE_LIST.count(get(ctx.message.guild.roles, name=rolename).id):
            if ctx.message.author.roles.count(get(ctx.message.guild.roles, name=rolename)) == 0:
                await discord.Member.add_roles(ctx.message.author,get(ctx.message.guild.roles, name=rolename))
                await ctx.send(get(ctx.message.guild.roles, name=rolename).name + ' assigned to ' + ctx.message.author.mention + ".")
            else:
                await ctx.send('You already have the role ' + get(ctx.message.guild.roles, name=rolename).name + ".")
        else:
            await ctx.send('This role does not exist on the list of selectable roles')

#Removes the selected role from the command caller if it exists.
@bot.command(name='removerole', help= 'Allows any users to remove themselves from any role on the bot\'s role list.')  
async def remRole(ctx, rolename):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if get(ctx.message.guild.roles, name=rolename) == None and any(char.isdigit() for char in rolename) == False:
        await ctx.send('This role does not exist on the list of selectable roles.')
    else:
        if any(char.isdigit() for char in rolename):
            if ctx.message.author.roles.count(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename))))) == 0:
                await ctx.send('You do not have this role')
            else:
                await discord.Member.remove_roles(ctx.message.author,get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))))
                await ctx.send(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).name + ' has been removed from your assigned roles.')
        elif bot.ROLE_LIST.count(get(ctx.message.guild.roles, name=rolename).id):
            if ctx.message.author.roles.count(get(ctx.message.guild.roles, name=rolename)) == 0:
                await ctx.send('You do not have this role')
            else:
                await discord.Member.remove_roles(ctx.message.author,get(ctx.message.guild.roles, name=rolename))
                await ctx.send(get(ctx.message.guild.roles, name=rolename).name + ' has been removed from your assigned roles.')
        else:
            await ctx.send('This role does not exist on the list of selectable roles.')

#ADMIN ONLY. Removes a role from the list of roles able to be set through the bot by any user.  
@bot.command(name='deleterole', help= 'Removes the role from the list of roles available to be picked by any members of this server')
async def delRole(ctx, rolename):
    if ctx.author.guild_permissions.administrator:
        if ctx.message.channel.id != bot.CHANNEL:
            return
        if get(ctx.message.guild.roles, name=rolename) == None and any(char.isdigit() for char in rolename) == False:
            await ctx.send('This role does not exist on the list of selectable roles.')
        else:
            if any(char.isdigit() for char in rolename):
                if bot.ROLE_LIST.count(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).id) == 0:
                    await ctx.send('Role does not exist on the list of self-assignable roles.')
                else:
                    bot.ROLE_LIST.remove(get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).id)
                    await ctx.send('Role ' + get(ctx.message.guild.roles, id=int("".join(filter(str.isdigit,rolename)))).name + ' removed from the list of freely selectable roles.')
            elif bot.ROLE_LIST.count(get(ctx.message.guild.roles, name=rolename).id):
                if bot.ROLE_LIST.count(get(ctx.message.guild.roles, name=rolename).id) == 0:
                    await ctx.send('Role does not exist on the list of self-assignable roles.')
                else:
                    bot.ROLE_LIST.remove(get(ctx.message.guild.roles, name=rolename).id)
                    await ctx.send('Role ' + get(ctx.message.guild.roles, name=rolename).name + ' removed from the list of freely selectable roles.')
            else:
                await ctx.send('Role does not exist on the list of self-assignable roles.')
    else:
        await ctx.send(ctx.author.mention + ", This command can only be run by an administrator.")

#Lets the user enable a feature to have the bot PM them in addition to mentions by the bot. The bot does not PM for mentions that happen when an errors while calling a command          
@bot.command(name='highnotify', help = 'Bot messages that mention you will also now send you a message directly (ONLY APPLIES TO PRICEWATCH).')          
async def highNotify(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if ctx.message.author.id in bot.HIGH_NOTIFY:
        await ctx.send(ctx.message.author.mention + ', You are already receiving direct messages for mentions.')
    else:
        bot.HIGH_NOTIFY.add(ctx.message.author.id)
        await ctx.send('High Notify enabled for you! You will now receive direct messages from me if I send a message that mentions you (Excluding command errors).')

#Stops the user from experiencing the highNotify feature in the function above        
@bot.command(name='removehighnotify', help = 'Stops the bot from messaging you directly if it sends a message that mentions you.')
async def removeHighNotify(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if ctx.message.author.id in bot.HIGH_NOTIFY == 0:
        await ctx.send('You are not currently on the list for receiving direct messages for mentions.')
    else:
        bot.HIGH_NOTIFY.discard(ctx.message.author.id)
        await ctx.send('High Notify disabled. You will no longer receive direct messages from me if I send a message that mentions you.')

#Adds a website domain to the list of websites that are considered safe and will not be sent to VirusTotal to be scanned.
@bot.command(name='whitelistAV', help = 'Whitelists a website from being scanned by VirusTotal. EX: Format should be "www.youtube.com", do not include https://, the / after .xxx, or anything past that.')
async def whitelistAV(ctx, link):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if ctx.author.guild_permissions.administrator:
        if "www." not in link:
            await ctx.send("Invalid link")
            return
        link = link.replace('\r','').replace('\n','')
        bot.WHITELISTED_SITES.add(link)
        await ctx.send("Link whitelisted.")
    else:
        await ctx.send(ctx.author.mention + ", This command can only be run by an administrator.")

#Removes a website domain from the list of websites that are considered safe. Calling this will not cause any previously sent messages to be scanned.
@bot.command(name='removewhitelistAV', help = 'Whitelists a website from being scanned by VirusTotal. EX: Format should be "www.youtube.com", do not include https://, the / after .xxx, or anything past that.')    
async def removeWhitelistAV(ctx, link):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if ctx.author.guild_permissions.administrator:
        if "www." not in link:
            await ctx.send("Invalid link")
            return
        link = link.replace('\r','').replace('\n','')
        bot.WHITELISTED_SITES.remove(link)
        await ctx.send("Link removed from whitelist.")
    else:
        await ctx.send(ctx.author.mention + ", This command can only be run by an administrator.")
    
######################################

#     PRICEWATCH & WEB-SCRAPING      #

######################################

#Scans the set of websites in the PRICE_LINKS variable for stock and price changes. Scans every X seconds going down the list rather than individually.
#Currently supported websites are: Newegg.com, Amazon.com, Microcenter.com, BHPhotoVideo.com, and Hottopic.com
#Scan interval is recommended to be 5s or greater.
#TODO: Change to have interval timers for each website, rather than one shared timer between all sites.
async def webscrape(bot):
    while True:
        bot.SAVEFLAG = False
        for x in bot.PRICE_LINKS:
            print('Checking websites...')
            if x in bot.NOTIFIED_LINKS:
                continue
            toextract = requests.get(x,headers=headers)
            toextract.raise_for_status()
            page = BeautifulSoup(toextract.text,'lxml')
            #TODO: Replace this with a switch-case function
            if "://www.newegg.com/" in x:
                if page.find(class_="product-inventory").find('strong').get_text() == "In stock.":
                    price = float(page.find(class_="price-current").find('strong').get_text() + page.find(class_="price-current").find('sup').get_text())
                    productname = page.find(class_="product-title").get_text().replace('\n','').replace('\t','')
                else:
                    await sleep(300)
                    continue
            elif "://www.amazon.com/" in x:
                if page.find(class_="a-size-medium a-color-price priceBlockBuyingPriceString") is None:
                    await sleep(300)
                    continue
                else:
                    price = float(page.find(class_="a-size-medium a-color-price priceBlockBuyingPriceString").get_text().replace('$',''))
                    productname = page.find(class_="a-size-large product-title-word-break").get_text().replace('\n','').replace('\t','')
            elif "://www.microcenter.com/" in x:
                if page.find(class_="StandardSku").find(id='pricing') is None:
                    await sleep(300)
                    continue
                else:
                    price = float(page.find(class_="StandardSku").find(id='pricing').get_text().replace('$',''))
                    productname = page.find(class_="mm-t007-title").h2.span.get("data-name")
            elif "://www.bhphotovideo.com/" in x:
                if page.find(class_="price_1DPoToKrLP8uWvruGqgtaY") is None or page.find(class_="toCartBtn_2C85cCSy-imVSRqkpuNDT2 buttonTheme_1mBX7Kocn_Oq_wzW6ri7s5") is None:
                    await sleep(300)
                    continue
                else:
                    price = float(page.find(class_="price_1DPoToKrLP8uWvruGqgtaY").get_text().replace('$',''))
                    productname = page.find(class_="title1_17KKS47kFEQb7ynVBsRb_5 reset_gKJdXkYBaMDV-W3ignvsP primary_ELb2ysditdCtk24iMBTUs").get_text().replace('\n','').replace('\t','')
            elif "://www.hottopic.com/" in x:
                if page.find(title="Sale Price").get_text() == "N/A":
                    await sleep(300)
                    continue  
                else:
                    price = float(page.find(title="Sale Price").get_text().split(" ")[0].replace('$',''))
                    productname = page.find(itemprop="name").get_text().replace('\n','').replace('\t','')
            toMention = ""
            for y in bot.PRICE_USERS.get(x).keys():
                if price <= float(bot.PRICE_USERS.get(x).get(y)):
                    toMention += (await bot.fetch_user(y)).mention + ' '
                    if y in bot.HIGH_NOTIFY:
                        notifTarget = await bot.fetch_user(y)
                        await notifTarget.send(notifTarget.mention + ' Product ' + productname + ' is in stock and at or under your requested price point.\nLINK: ' + x)           
            if bot.PRICE_HISTORY.get(x) is None:
                od = OrderedDict()
                od.update({datetime.now():price})
                bot.PRICE_HISTORY.update({x:od})
            else:
                od = bot.PRICE_HISTORY.get(x)
                od.update({datetime.now():price})
                bot.PRICE_HISTORY.update({x:od})
            if toMention == "":
                await sleep(10)
                continue
            else:
                await bot.get_channel(bot.CHANNEL).send(toMention + ' Product ' + productname + ' is in stock and at or under your requested price point.\nLINK: ' + x)
                bot.loop.create_task(notifiedlinks(bot,x))
                await sleep(10)
        bot.loop.create_task(modifyLinks(bot))
        await sleep(10)

#Automatically called by webscrape. A link that had a price or stock change detected will no longer be scanned for the next hour.        
async def notifiedlinks(bot,link):
    bot.NOTIFIED_LINKS.add(link)
    await sleep(3600)
    bot.NOTIFIED_LINKS.discard(link)

#Automatically called by webscrape. Will remove links inside of PRICE_USERS & PRICE_LINKS that are no longer being used.    
async def modifyLinks(bot):
    for x in bot.LINKS_TOREMOVE:
        bot.PRICE_LINKS.remove(x)
    for x in bot.LINKS_TOADD:
        bot.PRICE_LINKS.add(x)
    bot.SAVEFLAG = True
    bot.LINKS_TOREMOVE.clear()

#Adds a link to the list of links to be scanned by the bot. Has an option to check for stock only if the caller does not care about price.        
@bot.command(name='pricewatch',help= 'Adds a link for the bot to check for price and or stock changes of a product.')
async def pricewatch(ctx, link, price):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if "://www.newegg.com" not in link and "://www.amazon.com" not in link and "://www.microcenter.com" not in link and "://www.bhphotovideo.com" and "://www.hottopic.com/" not in link:
        await ctx.send('Sorry ' + ctx.message.author.mention + ', This website is currently not supported by this bot.')
        return
    if price.__str__().lower() == "stock":
        price = 9999999.99
    try: 
        price = float(price)
        if price < 0:
            await ctx.send(ctx.author.mention + ', The price you\'ve provided is invalid')
        if bot.PRICE_USERS.get(link) is not None:
            toUpdate = bot.PRICE_USERS.get(link)
            toUpdate.update({ctx.message.author.id:price})
            bot.PRICE_USERS.update({link:toUpdate})
            if price == 9999999.99:
                await ctx.send('You will now be notified if the product is in stock')
            else:
                await ctx.send('You will now be notified if the product drops below your target price')
        else:
            if link not in bot.LINKS_TOADD:
                bot.LINKS_TOADD.add(link)
            if link in bot.LINKS_TOREMOVE:
                bot.LINKS_TOREMOVE.remove(link)
            if(len(bot.PRICE_USERS)) == 0:
                bot.PRICE_USERS = {link:{ctx.message.author.id:price}}
            else:
                bot.PRICE_USERS.update({link:{ctx.message.author.id:price}})
            if price == 9999999.99:
                await ctx.send('I will now check the provided link for stock. You will also be notified if the product comes into stock')
            else:
                await ctx.send('I will now check the provided link for price changes. You will also be notified if the price for the product drops below your target price or if the product comes into stock if it is out of stock')
    except:
        await ctx.send(ctx.author.mention + ', The price you\'ve provided is invalid')

#Removes a link to the list of links to be scanned by the bot.
@bot.command(name='removepricewatch', help = 'Removes you from receiving notifications for a product you\'ve previously requested tracking for.')
async def removepricewatch(ctx, link):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if "://www.newegg.com/" not in link and "://www.amazon.com" not in link:
        await ctx.send(ctx.message.author.mention + ', you\'ve given an invalid link to check for removal.')
        return
    elif len(bot.PRICE_LINKS) == 0:
        await ctx.send(ctx.message.author.mention + ', Product is not currently being tracked. Nothing to remove.')
        return
    else:
        toUpdate = bot.PRICE_USERS.get(link)
        if toUpdate is None:
            await ctx.send(ctx.message.author.mention + ' , you are not on the notifications list for this product. Nothing to remove.')
            return
        toUpdate.pop(ctx.message.author.id, None)
        bot.PRICE_USERS.update({link:toUpdate})
        if len(toUpdate) == 0:
            bot.PRICE_USERS.pop(link)
            bot.LINKS_TOREMOVE.add(link)
            await ctx.send('You will no longer receive stock / price notifications for this product. I will no longer check for stock / prices for this product as no users are requesting checks on this item.')
        else:
            await ctx.send('You will no longer receive stock / price notifications for this product.')

#Sends a PNG graph of the price history of any item that was previously scanned by the bot. All data is stored in pricehistory.txt.            
@bot.command(name='pricehistory', help = 'Show statistics for an item that this bot has tracked in the past')       
async def priceHistory(ctx, link):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if bot.PRICE_HISTORY.get(link) is None:
        await ctx.send('Versedbot has no price history recorded for this product')
        return 
    else:
        rule = rrulewrapper(DAILY, interval=1)
        loc = RRuleLocator(rule)
        formatter = DateFormatter('%m/%d/%y')
        
        fig, ax = plt.subplots()
        x = bot.PRICE_HISTORY.get(link).keys()
        y = bot.PRICE_HISTORY.get(link).values()
        plt.plot_date(x,y,c="blue",linestyle="solid")
        ax.xaxis.set_major_locator(loc)
        ax.xaxis.set_major_formatter(formatter)
        ax.xaxis.set_tick_params(rotation=30, labelsize=10)
        ax.set_xlim(list(bot.PRICE_HISTORY.get(link).keys())[0],list(bot.PRICE_HISTORY.get(link).keys())[-1])
        plt.ylabel("Price in USD $")
        fig.suptitle('Price History')
        plt.grid(True)
        plt.savefig(fname='plot')
        with open('plot.png', 'rb') as fh:
            f = discord.File(fh, filename='plot.png')
        await ctx.send(file=f)
        tempDates = list(bot.PRICE_HISTORY.get(link).keys())
        tempPrices = list(bot.PRICE_HISTORY.get(link).values())
        tempDates.reverse()
        tempPrices.reverse()
        lowestPrice = min(list(bot.PRICE_HISTORY.get(link).values()))
        lowestDate = tempDates[tempPrices.index(lowestPrice)]
        commonPrice = mode(tempPrices)
        lastSale = None
        for x in tempPrices:
            if x < commonPrice:
                lastSale = x
                break
        if lastSale is None:
            lastSale = 'No recorded sales'
        else:
            lastSaleDate = tempDates.index(lastSale)
            lastSale = lastSale.__str__() + ' on the date ' + lastSaleDate.strftime("%m/%d/%Y") 
        await ctx.send(ctx.author.mention + ' Here is your requested price history graph for the link: ' + link + '\n```Statistics: \nMost Common Price: $' + commonPrice.__str__() + '\nLowest Recorded Price: $' + lowestPrice.__str__() + ' on the date ' + lowestDate.strftime("%m/%d/%Y") + '\nLatest Sale: ' + lastSale + '```')

#########################

# FFMPEG AUDIO PLAYBACK #

#########################

#Automatically run when the bot starts. Repeatedly checks for songs in audio queue to download and play whenever they are added.
async def playback(bot):       
    while True:
        if(len(bot.AUDIO_QUEUE) == 0):
            await sleep(3)
            continue
        video = bot.AUDIO_QUEUE.pop(0)
        if(os.path.exists("song.mp3")):
            os.remove("song.mp3")
        if(os.path.exists("song.flac")):
            os.remove("song.flac")
        if not(video[1]):
            options = {'format': 'bestaudio/best', 'extractaudio' : True, 'audioformat' : "mp3", 'outtmpl': 'song.mp3', 'noplaylist' : True}
        else:
            options = {'format': 'bestaudio/best', 'extractaudio' : True, 'audioformat' : "flac", 'outtmpl': 'song.flac', 'noplaylist' : True}
        with youtube_dl.YoutubeDL(options) as ydl:
            ydl.download([video[0]])
            videoInfo = ydl.extract_info(video[0], download=False)
        await bot.get_channel(bot.CHANNEL).send("I am now playing " + videoInfo.get('title', None))
        if not(video[1]):
            bot.CURR_VC_CHANNEL.play(discord.FFmpegPCMAudio("song.mp3"))
        else:
            bot.CURR_VC_CHANNEL.play(discord.FFmpegPCMAudio("song.flac"))
        bot.CURR_VC_CHANNEL.volume = 100
        while(bot.CURR_VC_CHANNEL.is_playing() or bot.CURR_VC_CHANNEL.is_paused()):
            await sleep(1)

#Connects the bot to the voice channel of whoever called the command from.         
@bot.command(name='connect', help = 'Connects the bot to your voice channel for audio playback.')        
async def connect(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    channels = get(ctx.bot.voice_clients, guild=ctx.guild)
    if(channels is None):
        bot.CURR_VC_CHANNEL = await ctx.message.author.voice.channel.connect()
    else:
        await ctx.send('I am already connected to a voice channel. If you are attempting to move me, please disconnect me first using !!disconnect.')

#Disconnects the bot from whichever channel it is in.    
@bot.command(name='disconnect', help = 'Disconnects the bot from its voice channel.')        
async def disconnect(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    channels = get(ctx.bot.voice_clients, guild=ctx.guild)
    if(channels is None):
        await ctx.send('I am not connected to any voice channel currently.')
    else:
        bot.AUDIO_QUEUE = list()
        await channels.disconnect()

#Downloads audio in lossy MP3 format. Saves data but has low quality audio and it tends to cut out more while playing than playaudioHiQ().
@bot.command(name='play', help = 'Plays the audio of a video from a link')     
async def playaudio(ctx, link):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if(bot.CURR_VC_CHANNEL == 0):
        await ctx.send("Please use !!connect to connect me to a voice channel before trying to play audio.")
        return
    if(link.contains("youtube.com") is False and link.contains("soundcloud.com") is False):
        await ctx.send("Invalid link, I can only play audio from Youtube and Soundcloud.")
    bot.AUDIO_QUEUE.append([link,False])
    await ctx.send("Requested song has been added to the queue")

#Downloads audio in lossless flac format. Uses more data but the audio quality is very good and there are little to no cuts in audio while playing back.
@bot.command(name='playHiQ', help = 'Plays the audio of a video from a link at the highest possible quality (Will take longer to download!)')       
async def playaudioHiQ(ctx, link):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if(bot.CURR_VC_CHANNEL == 0):
        await ctx.send("Please use !!connect to connect me to a voice channel before trying to play audio.")
        return
    bot.AUDIO_QUEUE.append([link,True])
    await ctx.send("Requested song has been added to the queue.")
    
#Skips to the next song in the queue.
@bot.command(name='audioskip', help = "Skips to the next song.")
async def nextSong(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if bot.CURR_VC_CHANNEL.is_playing() is False:
        ctx.send(ctx.author.mention() + ", Either the queue is empty or nothing is currently playing.")
        return
    bot.CURR_VC_CHANNEL.stop()
    if len(bot.AUDIO_QUEUE) == 0:
        bot.CURR_VC_CHANNEL.stop
        await ctx.send("No more songs in queue, stopping playback.")
    else:
        await ctx.send("Skipping to the next song.")

#Removes all instances of a song or link from the queue.
@bot.command(name='audioremove', help = 'Removes all instances of a link from the audio queue.')    
async def removeSong(ctx, link):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if(bot.AUDIO_QUEUE.count(link) == 0):
        await ctx.send(ctx.author.mention() + ", This song is not currently in the queue.")
        return
    list(filter(lambda a: a != link, bot.AUDIO_QUEUE))
    await ctx.send("Song has been removed from the queue.")

#Clears the entire queue. Does not stop playback of current song.
@bot.command(name='audioclear', help = 'Clears out the queue for songs/audio to be played by the bot.')    
async def clearQueue(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    bot.AUDIO_QUEUE.clear()
    await ctx.send("Song queue has been cleared.")

#Pauses playback of the current song.   
@bot.command(name='audiopause', help = 'Pauses playback of the current song/audio file.')
async def pauseAudio(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if bot.CURR_VC_CHANNEL.is_playing() is False:
        await ctx.send(ctx.author.mention + ", Either I'm already paused, not in the middle of playing a song, or not in a voice channel.")
        return
    bot.CURR_VC_CHANNEL.pause()
    await ctx.send("Playback paused.")

#Resumes playback of the current song. 
@bot.command(name='audioresume', help = 'Resumes playback of the current song/audio file.')    
async def resumeAudio(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if bot.CURR_VC_CHANNEL.is_playing() is False:
        await ctx.send(ctx.author.mention + ", Either I'm already paused, not playing any songs, or not in a voice channel.")
        return
    bot.CURR_VC_CHANNEL.resume
    await ctx.send("Resuming playback.")
    
###########################

#      SECRET SANTA       #

###########################

#Resets the bot's Secret Santa variables and allows for the event to be run again.
@bot.command(name='ssReset', help = "Admin only command. Clears the list of opted-in users and wishlists. Allows an admin to use !!ssStartEvent to start Secret Santa.")
async def secretSantaReset(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if ctx.author.guild_permissions.administrator:
        bot.SS_OPTED.clear()
        bot.SS_WISHLISTS.clear()
        bot.SS_PAIRS.clear()
        bot.SS_STARTED = False
        await ctx.send("Secret Santa has been reset! Use !!ssOptIn to sign up for the next Secret Santa event!")
    else:
        await ctx.send(ctx.author.mention + ", This command can only be run by an administrator.")

#Opts the user into a server's Secret Santa event.     
@bot.command(name='ssOptIn', help = 'Opts you in for Secret Santa!')
async def secretSantaOptIn(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if bot.SS_STARTED:
        await ctx.send(ctx.author.mention + ", Secret Santa has already begun or ended for this year. Please wait for an admin to reset before trying to opt in again.")
        return
    if ctx.author.id in bot.SS_OPTED:
        await ctx.send(ctx.author.mention + ", You're already opted in for Secret Santa.")
        return
    bot.SS_OPTED.add(ctx.author.id)
    await ctx.send("You're now opted in for Secret Santa!")

#Opts the user out of a server's Secret Santa event
@bot.command(name='ssOptOut', help = 'Removes you from the list for Secret Santa.')    
async def secretSantaOptOut(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if bot.SS_STARTED:
        await ctx.send(ctx.author.mention + ", Secret Santa has already begun and names have been distributed. If you need to opt out due to an emergency, please use !!ssPMGifter to send them a message.")
        return
    if (ctx.author.id in bot.SS_OPTED) is False:
        await ctx.send(ctx.author.mention + ", You're not currently participating in Secret Santa.")
        return
    bot.SS_OPTED.remove(ctx.author.id)
    if ctx.author.id in bot.SS_WISHLISTS:
        bot.SS_WISHLISTS.pop(ctx.author.id)
    await ctx.send("You have been opted out from participating in this server's Secret Santa.")

#ADMIN ONLY. Removes another user from the server's Secret Santa event.    
@bot.command(name='ssForceOut', help = 'Admin only command. Format: "command @user" Removes another person from the list for Secret Santa.')
async def secretSantaForceOut(ctx, target):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if bot.SS_STARTED:
        #TODO Add in code to redirect Gifter to the Giftee's giftee.
        await ctx.send(ctx.author.mention + ", Unfortunately, I currently cannot handle removing participating users while the event has already started. Please use !!ssReset.")
        return
    if ctx.author.guild_permissions.administrator:
        if(int("".join(filter(str.isdigit,target))) in bot.SS_OPTED):
            bot.SS_OPTED.remove(int("".join(filter(str.isdigit,target))))
            await ctx.send(target + ", You've been removed by an admin as a participant from this server's Secret Santa. If you believe this to be an error, please PM one of the admins directly.")
        else:
            await ctx.send(ctx.author.mention + ", This user isn't participating in Secret Santa.")
    else:
        await ctx.send(ctx.author.mention + ", This command can only be run by an administrator.")

#Updates a user's wishlist for their server's Secret Santa event.       
@bot.command(name='ssWishlist', help = 'Add up to 5 links for your Secret Santa wishlist')
async def secretSantaWishlist(ctx, *args):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if bot.SS_STARTED:
        await ctx.send(ctx.author.mention + ", Secret Santa has already started. Use !!ssPMGifter instead to send your gifter a message about changing your wishlist.")
    if len(args) > 5:
        await ctx.send(ctx.author.mention + ", You've given more than 5 links. Please limit your list to 5 or less.")
        return
    bot.SS_WISHLISTS.update({ctx.author.id:list(args)})
    await ctx.send("Your Secret Santa wishlist has been updated.")

#Starts the server's Secret Santa event
@bot.command(name='ssStartEvent', help = 'Starts the server Secret Santa event.')
async def startSecretSanta(ctx):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if not ctx.author.guild_permissions.administrator:
        await ctx.send(ctx.author.mention + ", This command can only be run by an administrator.")
        return
    if len(bot.SS_OPTED) < 3:
        await ctx.send(ctx.author.mention + ", Not enough participants, please wait for more users to opt in.")
        return
    if bot.SS_STARTED:
        await ctx.send(ctx.author.mention + ", The Secret Santa has already been started on this server.")
    noList = list()
    for gifter in bot.SS_OPTED: #Check for empty wishlists.
        templist = bot.SS_WISHLISTS.get(gifter)
        if(templist is None):        
            noList.append(gifter)
    if(len(noList) > 0):
        names = ""
        for i in noList:
            name = await bot.fetch_user(i)
            names = names + name.mention + "\n" 
        await ctx.send("The following participants have no items on their wishlist. \n" + names)
        return
    bot.SS_STARTED = True
    templist = copy.deepcopy(bot.SS_WISHLISTS)
    for gifter in bot.SS_OPTED: #Distribute pairings of names. 
        giftee = None
        while(giftee is None):
            giftee = random.choice(tuple(templist))
            if(bot.SS_PAIRS.get(giftee) == gifter or giftee == gifter):
                giftee = None
                continue
            else:
                templist.pop(giftee)
                bot.SS_PAIRS.update({gifter:giftee})
        gifterUser = await bot.fetch_user(gifter)
        gifteeUser = await bot.fetch_user(giftee)
        wishlist = bot.SS_WISHLISTS.get(giftee)
        await gifterUser.send("The Secret Santa event has started! Your Secret Santa Giftee is " + gifteeUser.name + ". Here is your Giftee's wishlist.")
        for i in wishlist:
            await gifterUser.send(i)
    await ctx.send(ctx.message.guild.default_role)
    await ctx.send("The Secret Santa event has started! Please contact an admin if there is an error with your giftees.")
    
#Sends a private message to a Secret Santa participant's gifter
@bot.command(name ='ssPMGifter', help = 'Sends a private message to your gifter')
async def pmGifter(ctx, message):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if (ctx.author.id in bot.SS_OPTED) is False:
        await ctx.send(ctx.author.mention + ", You're not currently participating in Secret Santa.")
        return
    if not bot.SS_STARTED:
        await ctx.send(ctx.author.mention + ", The Secret Santa event has not started yet on this server.")
        return
    
    gifter = await bot.fetch_user(list(bot.SS_PAIRS.keys())[list(bot.SS_PAIRS.values()).index(ctx.author.id)])
    await gifter.send('You have a message from your Giftee: "' + message + '"')
    
#Sends a private message to a Secret Santa participant's giftee
@bot.command(name ='ssPMGiftee', help = 'Sends a private message to your gifter')
async def pmGiftee(ctx, message):
    if ctx.message.channel.id != bot.CHANNEL:
        return
    if (ctx.author.id in bot.SS_OPTED) is False:
        await ctx.send(ctx.author.mention + ", You're not currently participating in Secret Santa.")
        return
    if not bot.SS_STARTED:
        await ctx.send(ctx.author.mention + ", The Secret Santa event has not started yet on this server.")
        return
    giftee = await bot.fetch_user(bot.SS_PAIRS.get(ctx.author.id))
    await giftee.send('You have a message from your Gifter: "' + message + '"')
    
bot.run(TOKEN)
